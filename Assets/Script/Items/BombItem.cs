using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class BombItem : Item
{
    private BoxCollider2D bombBox;
    private Animator bombAnim;
    public GameObject player;
    private SpriteRenderer sprite;
    public Sprite bombSpr;
    // Use this for initialization
    void Start()
    {
        bombBox = GetComponent<BoxCollider2D>();
        bombAnim = GetComponent<Animator>();
        sprite = GetComponent<SpriteRenderer>();

    }

    // Update is called once per frame
   

    public override void ActiveItem()
    {
        base.ActiveItem();
        bombBox.isTrigger = true;
        transform.position = player.transform.position;
        StartCoroutine("Explosion");

    }
    public override void DeactiveItem()
    {
        base.DeactiveItem();
        sprite.sprite = bombSpr;
        bombBox.isTrigger = false;
        bombBox.size = new Vector2(0.4f, 0.2f);
        bombBox.offset = new Vector2(0, 0);
        bombAnim.enabled = false;
        gameObject.SetActive(false);

    }



    IEnumerator Explosion()
    {
        yield return new WaitForSeconds(1f);
        bombAnim.enabled = true;
        bombBox.size = new Vector2(0.1f, bombBox.size.y / 2);
        bombBox.offset -= new Vector2(0, 0.5f);
        yield return new WaitForSeconds(1f);
        DeactiveItem();
    }

  
}
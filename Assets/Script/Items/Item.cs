using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    private float existTime;
    private float effectTime;
    private bool isActive;
    // Use this for initialization
    void Start()
    {
        isActive = false;

    }

    // Update is called once per frame
   
    public virtual void ActiveItem()
    {
        gameObject.SetActive(true);
        isActive = true;

    }
    public virtual void DeactiveItem()
    {
        isActive = false;
    }

   

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {

            gameObject.SetActive(false);
        }

    }
    public bool IsActive
    {
        get
        {
            return isActive;
        }
        set
        {
            isActive = value;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class MonsterMovement : MonoBehaviour
{
    public UnityEvent beKilled;
    private float distToX;
    private RaycastHit2D hit;
    private int dir;
    private float speed = 0.02f;
    public static bool isStunned;
    // Use this for initialization
    void Start()
    {
        isStunned = false;
        int x = Random.Range(0, 3);
        if (x % 2 == 0) transform.localEulerAngles = new Vector2(0, 0);
        else { transform.localEulerAngles = new Vector2(0, 180); }
        distToX = GetComponent<Collider2D>().bounds.extents.x;
        if (transform.localEulerAngles.y == 0) dir = 1;
        else dir = -1;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        ChangeDir();
        if (!isStunned)
        {
            Move();
        }
        GameLogic.AcrossScreen(transform);
    }

    void Move()
    {
        transform.position = Vector3.Lerp(transform.position, transform.position + transform.right, speed);

    }
    void ChangeDir()
    {
        hit = Physics2D.Raycast(transform.position + new Vector3(distToX + 0.01f, 0, 0) * dir, transform.right, 0.05f);
        if (hit.collider != null && !hit.collider.gameObject.CompareTag("DeadZone") && !hit.collider.gameObject.CompareTag("Player"))
        {
            transform.eulerAngles += new Vector3(0, 180f, 0);
            dir = -dir;
        }
    }


    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("AttackPath"))
        {
            beKilled.Invoke();
            gameObject.SetActive(false);
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Rock"))
        {
            gameObject.SetActive(false);

        }
    }
    public float Speed
    {
        get
        {
            return speed;
        }
        set
        {
            speed = value;
        }
    }
    public void BeStunned()
    {
        isStunned = true;
        StartCoroutine("StunOver");
    }

    IEnumerator StunOver()
    {
        yield return new WaitForSeconds(5);
        isStunned = false;


    }


}


using UnityEngine;
using System.IO;
using UnityEngine.UI;
public class TestSo : MonoBehaviour
{
    public Text text;
    public SOData data;
    // Use this for initialization
    void Start()
    {
      

        LoadData();
        text.text = "" + data.level;

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) {
            ChangeLevel();
            SaveData();
        }
    }
    public void ChangeLevel()
    {
        data.level++;
        text.text = "" + data.level;
    }
    void SaveData() {
       
        data.playedTutorial = true;
        string json = JsonUtility.ToJson(data, true);
        StreamWriter sw = File.CreateText(Application.dataPath + "/a.json");
        print(json);
        sw.Close();
        File.WriteAllText(Application.dataPath + "/a.json", json);
    }
    void LoadData() {       
        string dataAsJson = File.ReadAllText(Application.dataPath + "/a.json");
        JsonUtility.FromJsonOverwrite(dataAsJson, data);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerInput : MonoBehaviour
{
    private bool isForward, isBack, isAttack, isJump, isUsing, itemActive;
    private bool canControl = true;
    private Button leftBtn, rightBtn, jumpBtn, usingItemBtn, attackBtn;
    // Use this for initialization


    // Update is called once per frame
    void Update()
    {
        if (canControl) CheckInput();
    }

    void CheckInput()
    {
        if (Input.GetKey(KeyCode.A) || Input.GetAxis("LeftJoyH") < 0)
        {
            isBack = true;
        }
        if (!Input.GetKey(KeyCode.A) && Input.GetAxis("LeftJoyH") == 0)
        {
            isBack = false;
        }
        if (Input.GetKey(KeyCode.D) || Input.GetAxis("LeftJoyH") > 0)
        {
            isForward = true;
        }
        if (!Input.GetKey(KeyCode.D) && Input.GetAxis("LeftJoyH") == 0)
        {
            isForward = false;
        }
        if (Input.GetKey(KeyCode.G) || Input.GetAxis("XButton") != 0)
        {
            if (!isUsing) isAttack = true;
            else
            {
                itemActive = true;
            }
        }
        if (!Input.GetKey(KeyCode.G) && Input.GetAxis("XButton") == 0)
        {
            isAttack = false;
            itemActive = false;
        }
        if (Input.GetKey(KeyCode.Space) || Input.GetAxis("AButton") != 0)
        {

            isJump = true;

        }
        if (!Input.GetKey(KeyCode.Space) && Input.GetAxis("AButton") == 0)
        {
            isJump = false;
        }
        if (Input.GetKey(KeyCode.S) || Input.GetAxis("LeftJoyV") != 0)
        {
            isAttack = false;
            isUsing = true;
        }
        if (!Input.GetKey(KeyCode.S) && Input.GetAxis("LeftJoyV") == 0)
        {
            isUsing = false;
            itemActive = false;
        }
    }
    void MobileInput()
    {




    }
    public bool IsForward
    {
        get
        {
            //Some other code
            return isForward;
        }
        set
        {
            //Some other code
            isForward = value;
        }
    }
    public bool IsJump
    {
        get
        {
            //Some other code

            return isJump;

        }
        set
        {
            //Some other code
            isJump = value;
        }
    }
    public bool IsBack
    {

        get
        {
            //Some other code
            return isBack;
        }
        set
        {
            //Some other code
            isBack = value;
        }
    }
    public bool IsAttack
    {
        get
        {
            //Some other code
            return isAttack;
        }
        set
        {
            //Some other code
            isAttack = value;
        }
    }
    public bool ItemActive
    {
        get
        {
            //Some other code
            return itemActive;
        }
        set
        {
            //Some other code
            itemActive = value;
        }
    }
    public bool CanControl
    {
        get
        {
            //Some other code
            return canControl;
        }
        set
        {
            //Some other code
            canControl = value;
        }
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class PlayerAttack : MonoBehaviour
{
    private float cooldown = 0.3f;
    private bool isAttacking;
    private Transform attackPathTr;   // Get the child for Attacking, it need to be trigger
    public UnityEvent playerAttack;
    // Use this for initialization
    void Start()
    {
        isAttacking = false;
        attackPathTr = transform.GetChild(0);
        attackPathTr.gameObject.SetActive(false);

    }
   
    // Update is called once per frame
    void Update()
    {
        if (isAttacking)
        {
            cooldown -= Time.deltaTime;

        }
        CooldownEffect();
    }
    public bool IsAttack
    {
        get
        {
            return isAttacking;
        }
        set
        {
            isAttacking = value;
        }



    }


   public void Attack()
    {
        
            isAttacking = true;
            attackPathTr.gameObject.SetActive(true);
            playerAttack.Invoke();
            attackPathTr.localPosition += new Vector3(5, 0, 0);  //set box go right a little
        
    }
    private void CooldownEffect() {
        if (cooldown <= 0.25f && attackPathTr.position.x != 0)
        {
            attackPathTr.localPosition = new Vector2(0f, 0f);
            attackPathTr.gameObject.SetActive(false);

        }

        if (cooldown <= 0)
        {
            isAttacking = false;
            cooldown = 0.3f;
            //set box originally.
            PlayerController.enableMovement = true;
        }

    }

}

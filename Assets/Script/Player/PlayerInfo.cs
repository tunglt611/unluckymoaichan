using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerInfo : MonoBehaviour
{
    private int currentLevel;
    private int highestLevel;
    //private int currentScore;
    //private int highestScore;
    private int amountOfGold;
    private int amountOfLife = 5;
    public Dictionary<string, int> listItem;
    private string usingItem;
    // Use this for initialization

    void Awake()
    {
        currentLevel = !PlayerPrefs.HasKey("CurrentLevel") ? 1 : PlayerPrefs.GetInt("CurrentLevel");

        highestLevel = !PlayerPrefs.HasKey("HighestLevel") ? 1 : PlayerPrefs.GetInt("HighestLevel");

        usingItem = !PlayerPrefs.HasKey("UsingItem") ? "Bomb" : PlayerPrefs.GetString("UsingItem");

        amountOfLife = !PlayerPrefs.HasKey("AmountOfLife") ? 5 : PlayerPrefs.GetInt("AmountOfLife");

        amountOfGold = !PlayerPrefs.HasKey("AmountOfGold") ? 1500 : PlayerPrefs.GetInt("AmountOfGold");

        listItem = new Dictionary<string, int>();

        if (!PlayerPrefs.HasKey("Bomb"))
        {
            listItem.Add("Bomb", 0);
            listItem.Add("Jump", 0);
            listItem.Add("Stun", 0);
        }
        else
        {
            listItem.Add("Bomb", PlayerPrefs.GetInt("Bomb"));
            listItem.Add("Jump", PlayerPrefs.GetInt("Jump"));
            listItem.Add("Stun", PlayerPrefs.GetInt("Stun"));
        }
        SaveGame();
    }

    public int AmountOfLife
    {
        get
        {
            return amountOfLife;
        }
        set
        {
            amountOfLife = value;
        }
    }
    public int AmountOfGold
    {
        get
        {
            return amountOfGold;
        }
        set
        {
            amountOfGold = value;
        }
    }
    public int CurrentLevel
    {
        get
        {
            return currentLevel;
        }
        set
        {
            currentLevel = value;
        }
    }

    public int HighestLevel
    {
        get
        {
            return highestLevel;
        }
        set
        {
            highestLevel = value;
        }
    }


    public string UsingItem
    {
        get
        {
            return usingItem;
        }
        set
        {
            usingItem = value;
        }
    }

    public void UsedItem()
    {

        listItem[usingItem]--;
    }
    public void SaveGame()
    {
        List<string> listKeyName = new List<string>(listItem.Keys);
        PlayerPrefs.SetString("UsingItem", usingItem);
        PlayerPrefs.SetInt("CurrentLevel", currentLevel);
        PlayerPrefs.SetInt("HighestLevel", highestLevel);

        foreach (string name in listKeyName)
        {
            PlayerPrefs.SetInt(name, listItem[name]);
        }
        PlayerPrefs.SetInt("AmountOfGold", amountOfGold);
        PlayerPrefs.SetInt("AmountOfLife", amountOfLife);
        PlayerPrefs.Save();

    }
}

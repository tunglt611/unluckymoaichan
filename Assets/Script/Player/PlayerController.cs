using System.Collections;
using UnityEngine;
using UnityEngine.Events;
public class PlayerController : MonoBehaviour
{

    public static bool enableMovement;
    private GameLogic logic;
    private Rigidbody2D m_rigid;
    //Move
    private readonly float speed = 2.3f;
    private bool OnGround;
    private Transform fireTr;

    //Jump

    public BombItem bomb;
    private float jumpVel;

    // Animation
    private Animator m_anim;
    private PlayerInput playerInput;
    private PlayerAttack playerAttack;
    private PlayerInfo info;
    //sound




    public UnityEvent usingBombEvent, playerDead,
        jumpEvent, usingStunEve,
        updateDropdown, updateLife;



    void Start()
    {
        logic = ScriptableObject.CreateInstance<GameLogic>();
        m_rigid = GetComponent<Rigidbody2D>();
        OnGround = false;
        jumpVel = 9.3f;
        m_anim = GetComponent<Animator>();
        fireTr = transform.GetChild(1);
        enableMovement = true;
    }

    // Update is called once per frame


    void FixedUpdate()
    {
        if (!OnGround)
        {
            logic.SmoothJump(m_rigid);
        }

        if (enableMovement)
        {
            Move();
            GameLogic.AcrossScreen(transform);
            Attack();
        }
        else
        {
            m_rigid.velocity = new Vector2(0, m_rigid.velocity.y);
        }
        GameLogic.AcrossScreen(playerAttack.transform);

    }

    void Update()
    {
        UsingItem();
    }

    void Move()
    {
        // make smooth jump
        if (!playerInput.IsForward && !playerInput.IsBack)
        {
            m_rigid.velocity = new Vector2(0, m_rigid.velocity.y);
            if (OnGround) m_anim.SetInteger("dinoControl", 0);
        }
        else
        {
            if (playerInput.IsForward)
            {
                m_rigid.velocity = new Vector2(speed, m_rigid.velocity.y);
                transform.eulerAngles = new Vector3(0, 0, 0);
                if (OnGround) m_anim.SetInteger("dinoControl", 2);
                fireTr.localPosition = new Vector3(fireTr.localPosition.x, fireTr.localPosition.y, -1);
            }
            if (playerInput.IsBack)
            {
                m_rigid.velocity = new Vector2(-speed, m_rigid.velocity.y);
                transform.eulerAngles = new Vector3(0, 180, 0);
                if (OnGround) m_anim.SetInteger("dinoControl", 2);
                fireTr.localPosition = new Vector3(fireTr.localPosition.x, fireTr.localPosition.y, 1);
            }
        }


        if (playerInput.IsJump)
        {
            if (OnGround) m_rigid.velocity = Vector2.up * jumpVel;
            m_anim.SetInteger("dinoControl", 1);
            jumpEvent.Invoke();
        }
    }



    void UsingItem()
    {
        string usingItem = info.UsingItem;
        if (playerInput.ItemActive && OnGround && enableMovement)
        {
            switch (usingItem)
            {
                case "Bomb":
                    if (usingBombEvent != null && info.listItem["Bomb"] > 0 && !bomb.IsActive)
                    { //using Event to make an event then add the bomb handle this event.
                        bomb.ActiveItem();
                        usingBombEvent.Invoke();
                        updateDropdown.Invoke();//   uiManager.UpdateDropdown();
                                                //  eff.PlayEffect("usingItem");
                    }
                    break;

                case "Jump":
                    if (info.listItem["Jump"] > 0)
                    {
                        //   info.jumpItem.GetComponent<>();
                    }
                    break;

                case "Stun":
                    if (usingStunEve != null && info.listItem["Stun"] > 0 && MonsterMovement.isStunned == false)
                    {
                        usingStunEve.Invoke();
                        updateDropdown.Invoke();// uiManager.UpdateDropdown();
                                                //   eff.PlayEffect("usingItem");
                    }
                    break;
            }
        }
    }


    void Attack()
    {
        if (playerInput.IsAttack && !playerAttack.IsAttack)
        {
            playerAttack.Attack();
            enableMovement = false;
         }

    }


    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Ground") || collision.gameObject.CompareTag("Rock"))
        {
            OnGround = true;
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Ground") || collision.gameObject.CompareTag("Rock"))

        {
            OnGround = false;
        }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("DeadZone"))
        {
            RipPlayer();

        }
        if (collision.gameObject.CompareTag("BombItem"))
        {
            info.listItem["Bomb"]++;
            updateDropdown.Invoke();

        }
        if (collision.gameObject.CompareTag("JumpItem"))
        {
            info.listItem["Jump"]++;
            updateDropdown.Invoke();
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Object"))
        {
            transform.parent = null;
        }
    }

    public void RipPlayer()
    {
        if (enableMovement)
        {
            enableMovement = false;
            m_anim.Play("Dead", 0);
            transform.GetChild(1).gameObject.SetActive(false);
            transform.GetChild(0).gameObject.SetActive(false);
            m_rigid.constraints = RigidbodyConstraints2D.FreezeAll;
            updateLife.Invoke();// uiManager.UpdateLife();
                                // eff.PlayEffect("dead");
            StartCoroutine("PlayerDead");
        }
    }



    IEnumerator PlayerDead()
    {
        yield return new WaitForSeconds(3);
        playerDead.Invoke();

    }
    void OnDisable()
    {
        Destroy(playerInput);
        Destroy(info);
        playerAttack.enabled = false;
    }
    void OnEnable()
    {
        gameObject.AddComponent<PlayerInput>();
        playerInput = GetComponent<PlayerInput>();
        gameObject.AddComponent<PlayerInfo>();
        info = GetComponent<PlayerInfo>();
        playerAttack = GetComponent<PlayerAttack>();
        playerAttack.enabled = true;

    }
    public void EnableMovement(bool value)
    {

        enableMovement = value;

    }
    public void UpdateInfo()
    {
        info.UsedItem();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class MusicManager : MonoBehaviour
{
    [Range(0f, 100f)] private static float bgmVolume = 50f;
    [Range(0f, 100f)] public static float soundVolume = 60f;
    [HideInInspector] public static bool AudioBegin = false;
    private Slider bgmSlider, soundSlider;
    private GameObject dontObj;
    public SoundEff eff;
    int x;
    public AudioSource bgm;
        // Use this for initialization

    void Start()
    {
        
        x = 0;

        if (gameObject.name.Equals("BGMSlider"))
        {
            bgmSlider = gameObject.GetComponent<Slider>();
            bgmSlider.value = bgmVolume;

        }
        if (gameObject.name.Equals("SoundSlider"))
        {
            soundSlider = gameObject.GetComponent<Slider>();
            soundSlider.value = soundVolume;
        }

        if (AudioBegin && SceneManager.GetActiveScene().name == "GameMenu") {
            bgm.enabled = false;

        }
            if ((!AudioBegin) && SceneManager.GetActiveScene().name == "GameMenu")
        {
            AudioBegin = true;
            DontDestroyOnLoad(gameObject);
            SetVolume();
            dontObj = gameObject;
        }
      
    }

    // Update is called once per frame
    void Update()
    {

        if ((SceneManager.GetActiveScene().name == "Level1" || SceneManager.GetActiveScene().name == "Level2") && x < 10)
        {
            AudioBegin = false;
            Destroy(dontObj);
            ChangeVolume();
            x++;
        }

    }
    public void ChangeVolume()
    {
        if (bgmSlider != null)
        {
            bgmVolume = bgmSlider.value;
            SetVolume();
        }

        if (soundSlider != null)
        {
            soundVolume = soundSlider.value;
            SetSoundEff();
        }

    }
    void SetVolume()
    {
       GameObject.Find("BGM").GetComponent<AudioSource>().volume = bgmVolume / 100f;
    }



    void SetSoundEff()
    {
        if (SceneManager.GetActiveScene().name == "Level1" || SceneManager.GetActiveScene().name == "Level2")
            eff.source.volume = soundVolume / 100f;
    }

}

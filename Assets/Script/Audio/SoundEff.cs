using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEff : MonoBehaviour
{
    public AudioClip jump, eatThings, attack, dead, usingItem;
    public AudioSource source;

    // Use this for initialization
    void Start()
    {
        source.volume = MusicManager.soundVolume;
    }

    // Update is called once per frame
  

    public void PlayEffect(string name)
    {
        switch (name)
        {
            case "jump":
                source.clip = jump;
             if(!source.isPlaying)  source.Play();
                break;
            case "eatThings":
                source.clip = eatThings;
                source.Play();
                break;
            case "attack":
                source.clip = attack;
                 source.Play();
                break;
            case "dead":
                source.clip = dead;
                if (!source.isPlaying) source.Play();
                break;
            case "usingItem":
                source.clip = usingItem;
                if (!source.isPlaying) source.Play();
                break;
               

        }

    }
}

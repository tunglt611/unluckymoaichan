using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLogic : ScriptableObject
{
    private readonly float fallMultiplier = 2.5f;
    private readonly float lowJumpMultiplier = 2f;
    // Use this for initialization

    public static void AcrossScreen(Transform trans)
    {
        if (trans.position.x < -9) trans.position += new Vector3(18, 0, 0);
        if (trans.position.x > 9) trans.position += new Vector3(-18, 0, 0);
    }

    public void SmoothJump(Rigidbody2D m_rigid)
    {
        if (m_rigid.velocity.y < 0)
        {
            m_rigid.velocity += Vector2.up * Physics2D.gravity * (fallMultiplier - 1) * Time.deltaTime;

        }
        else
        {
            m_rigid.velocity += Vector2.up * Physics2D.gravity * (lowJumpMultiplier - 1) * Time.deltaTime;

        }
    }
}

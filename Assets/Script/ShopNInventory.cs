using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ShopNInventory : MonoBehaviour
{
    public Text bombTxt, stunTxt, goldTxt;
    private bool bombS, stunS, bombI, stunI;
    public Image bombSImage, stunSImage, bombIImage, stunIImage;
    public Button buyBtn, sellBtn;
    // Use this for initialization
    void Start()
    {
        UpdateData();
        buyBtn.interactable = false;
        sellBtn.interactable = false;
    }
   
  
    // Update is called once per frame
    public void UpdateData()
    {
        bombTxt.text = "X" + PlayerPrefs.GetInt("Bomb", 0);
        stunTxt.text = "X" + PlayerPrefs.GetInt("Stun", 0);
        goldTxt.text = PlayerPrefs.GetInt("AmountOfGold", 0) + "$";
    }
    public void ChooseItem(GameObject gameObject)
    {
        if (gameObject.name.Equals("BombSImage"))
        {
            bombS = true; stunS = false; bombI = false; stunI = false;
            CheckButton();
            bombSImage.color = Color.yellow;
            stunSImage.color = Color.white;
            bombIImage.color = Color.white;
            stunIImage.color = Color.white;
        }
        if (gameObject.name.Equals("StunSImage"))
        {
            bombS = false; stunS = true; bombI = false; stunI = false;
            CheckButton();
            bombSImage.color = Color.white;
            stunSImage.color = Color.yellow;
            bombIImage.color = Color.white;
            stunIImage.color = Color.white;
        }
        if (gameObject.name.Equals("BombIImage"))
        {
            bombS = false; stunS = false; bombI = true; stunI = false;
            CheckButton();
            bombSImage.color = Color.white;
            stunSImage.color = Color.white;
            bombIImage.color = Color.yellow;
            stunIImage.color = Color.white;

        }
        if (gameObject.name.Equals("StunIImage"))
        {
            bombS = false; stunS = false; bombI = false; stunI = true;
            CheckButton();
            bombSImage.color = Color.white;
            stunSImage.color = Color.white;
            bombIImage.color = Color.white;
            stunIImage.color = Color.yellow;
        }
    }

    public void BuyItem()
    {
        int gold = PlayerPrefs.GetInt("AmountOfGold");
        if (bombS && gold >= 300)
        {
            PlayerPrefs.SetInt("Bomb", PlayerPrefs.GetInt("Bomb") + 1);
            PlayerPrefs.SetInt("AmountOfGold", gold - 300);
        }
        if (stunS && gold >= 500)
        {
            PlayerPrefs.SetInt("Stun", PlayerPrefs.GetInt("Stun") + 1);
            PlayerPrefs.SetInt("AmountOfGold", gold - 500);
        }
        PlayerPrefs.Save();
        UpdateData();
    }
    public void SellItem()
    {
        int gold = PlayerPrefs.GetInt("AmountOfGold");
        if (bombI && PlayerPrefs.GetInt("Bomb") > 0)
        {
            PlayerPrefs.SetInt("Bomb", PlayerPrefs.GetInt("Bomb") - 1);
            PlayerPrefs.SetInt("AmountOfGold", gold + 150);
        }
        if (stunI && PlayerPrefs.GetInt("Stun") > 0)
        {
            PlayerPrefs.SetInt("Stun", PlayerPrefs.GetInt("Stun") - 1);
            PlayerPrefs.SetInt("AmountOfGold", gold + 250);
        }
        PlayerPrefs.Save();
        UpdateData();
    }
    void CheckButton()
    {
        if (bombS || stunS)
        {
            buyBtn.interactable = true; sellBtn.interactable = false;
        }
        if (bombI || stunI)
        {
            buyBtn.interactable = false; sellBtn.interactable = true;
        }
    }

}

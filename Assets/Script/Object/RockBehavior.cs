using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockBehavior : MonoBehaviour
{
    private Rigidbody2D rock_rigid;
    private float timeToMove = 1f;
    private bool active;
    private float attackSpeed = 20f;
    private float originalMass = 100f;
    private RaycastHit2D leftRay, rightRay;
    private BoxCollider2D rockBox;
    private Vector2 currentSpeed;
    //    private PlayerInput playerInput;
    // Use this for initialization
    void Start()
    {
        rockBox = GetComponent<BoxCollider2D>();
        rock_rigid = GetComponent<Rigidbody2D>();


    }

    // Update is called once per frame
    void Update()
    {
        if (active)
        {
            timeToMove -= Time.deltaTime;
        }
        if (rock_rigid.velocity == Vector2.zero) rock_rigid.mass = originalMass;
        PushRock();
       GameLogic.AcrossScreen(transform);

    }

  
    void OnTriggerEnter2D(Collider2D collision)
    {


        if (collision.gameObject.CompareTag("AttackPath"))
        {
            leftRay = Physics2D.Raycast(new Vector2(transform.position.x - rockBox.bounds.extents.x, transform.position.y), -transform.right * 20);
            rightRay = Physics2D.Raycast(new Vector2(transform.position.x + rockBox.bounds.extents.x, transform.position.y), transform.right * 20);



            float dir = (transform.position.x - collision.transform.position.x);
            rock_rigid.mass = 0.0f;
          

            if (dir > 0)
            {
                if (rightRay.distance > 0.1f|| rightRay.distance==0)
                {
                  
                    rock_rigid.velocity = new Vector2(attackSpeed, 0);
                }
            }
            else
            {

                if (leftRay.distance > 0.1f || leftRay.distance == 0)
                {
                    rock_rigid.velocity = new Vector2(-attackSpeed, 0);
                }
            }
            currentSpeed = rock_rigid.velocity;
        }

    }


    void PushRock()
    {
        if (timeToMove < 0)
        {
            rock_rigid.mass = 10f;
        }


    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground") || collision.gameObject.CompareTag("Object"))
        {
            rock_rigid.velocity = Vector2.zero;
            rock_rigid.mass = originalMass * 10;
        }
        if (collision.gameObject.CompareTag("Player"))
        {
            active = true;

        }
        if (collision.gameObject.CompareTag("DeadZone"))
        {
            if (rock_rigid.velocity.x > 0) rock_rigid.velocity = currentSpeed;
            else rock_rigid.velocity = currentSpeed;

        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            active = false;
            timeToMove = 1.5f;
        }
    }
}




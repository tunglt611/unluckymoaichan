using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructibleObject : MonoBehaviour
{
    // Use this for initialization

    // Update is called once per frame

    void OnTriggerStay2D(Collider2D collision)
    {

        if (collision.gameObject.CompareTag("AttackPath"))
        {
            gameObject.SetActive(false);

        }

    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("BombItem"))
        {
            gameObject.SetActive(false);

        }
    }



}

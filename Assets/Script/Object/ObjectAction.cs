using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class ObjectAction : MonoBehaviour
{
    public UnityEvent beAte;
    float speedRotation;
    private int dir;
    // Use this for initialization
    void Start()
    {
        speedRotation = Random.Range(3f, 20f);
        int rand = Random.Range(0, 3);

        if (rand % 2 == 0) dir = 1;
        else dir = -1;
    }

    // Update is called once per frame
    void Update()
    {
        transform.eulerAngles = Vector3.Lerp(transform.eulerAngles, transform.eulerAngles + new Vector3(0, 0, speedRotation * dir), 0.5f);
    }


    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {

            if (isActiveAndEnabled) { beAte.Invoke(); }
            gameObject.SetActive(false);
        }
    }
}

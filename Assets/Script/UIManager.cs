using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
public class UIManager : MonoBehaviour
{
    public Text amountOfLife;
    public Text time;
    private float timeF = 20;
    public Dropdown dropDownItem;
    private PlayerInfo info;
    public UnityEvent timeOver;
    private bool isPlayerWon = false;

    public bool IsPlayerWon
    {
        get
        {
            return isPlayerWon;
        }

        set
        {
            isPlayerWon = value;
        }
    }

    // Use this for initialization
    void Start()
    {
        if (SceneManager.GetActiveScene().name == "Level1") timeF = 50;
        if (SceneManager.GetActiveScene().name == "Level2") timeF = 100;
        info = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerInfo>();
        amountOfLife.text = "X" + info.AmountOfLife.ToString();
        time.text = "Time: " + timeF;

        //Dropdown

        UpdateDropdown();
    }

    // Update is called once per frame
    void Update()
    {
        if (timeF > 0)
        {
            timeF -= Time.deltaTime;
        }
        else
        {
            if (!IsPlayerWon)
            {
                timeOver.Invoke();
                timeF = 0;
            }
        }
        time.text = "Time: " + Mathf.RoundToInt(timeF);

    }


    void ChangeItem()
    {
        int x = dropDownItem.value;
        switch (x)
        {
            case 0:
                info.UsingItem = "Bomb";
                break;
            case 1:
                info.UsingItem = "Stun";
                break;

        }

    }
    public void UpdateDropdown()
    {
        dropDownItem.options[0].text = "x" + info.listItem["Bomb"].ToString();
        dropDownItem.options[1].text = "x" + info.listItem["Stun"].ToString();
        dropDownItem.captionText.text = dropDownItem.options[dropDownItem.value].text;
        switch (info.UsingItem)
        {
            case "Bomb": dropDownItem.value = 0; break;
            case "Stun": dropDownItem.value = 1; break;


        }
    }
    public void UpdateLife()
    {
        amountOfLife.text = "X" + info.AmountOfLife.ToString();
    }

}

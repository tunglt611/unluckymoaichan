using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class Tutorial : MonoBehaviour
{
    private GameObject left, right, jump, attack, useItem;
    private PlayerInput input;
    [SerializeField] private Transform playerTr;
    [SerializeField] private GameObject image;
    private float pressingTime;
    private bool isPressing, down;
    private int actionTime = 0;
    public Text text; 
    // Use this for initialization
    void Start()
    {
        pressingTime = 0f;
        down = false;
        input = playerTr.gameObject.GetComponent<PlayerInput>();
        right = transform.GetChild(0).gameObject;
        left = transform.GetChild(1).gameObject;
        jump = transform.GetChild(2).gameObject;
        attack = transform.GetChild(3).gameObject;
        useItem = transform.GetChild(4).gameObject;
        right.SetActive(false);
        left.SetActive(true);
        jump.SetActive(false);
        attack.SetActive(false);
        useItem.SetActive(false);
        //    input.CanControl = false;
        //   StartCoroutine("TutorialMove");

    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position = playerTr.position;
        text.transform.position = new Vector3(-1,1,0);
        if (isPressing) pressingTime += Time.deltaTime;
        GuideTutorial();

    }




    IEnumerator TutorialMove()
    {

        input.CanControl = false;
        yield return new WaitForSeconds(4);
        image.transform.position = new Vector3(0, 0, 0);
        image.SetActive(true);
        yield return new WaitForSeconds(3);
        PlayerPrefs.DeleteAll();
        input.CanControl = true;
        SceneManager.LoadScene("Level1");
    }


    void GuideTutorial()
    {
        if (left.activeSelf)
        {
            text.text = "Press in 2s";
            if (input.IsBack)
            {
                isPressing = true;
                if (pressingTime >= 2f)
                {
                    isPressing = false;

                    left.SetActive(false);
                    right.SetActive(true);
                }
            }
            else
            {

                isPressing = false;
                pressingTime = 0f;
            }
        }



        if (right.activeSelf)
        {
            if (input.IsForward)
            {
                isPressing = true;
                if (pressingTime >= 2f)
                {
                    isPressing = false;
                    right.SetActive(false);
                    jump.SetActive(true);

                }
            }
            else
            {
                isPressing = false;
                pressingTime = 0f;
            }
        }

        if (jump.activeSelf)
        {
            text.text = "Press 5 times";
            if (input.IsJump && !down)
            {
                down = true;
                actionTime++;
            }
            if (!input.IsJump)
            { down = false; }
            if (actionTime == 5)
            {
                attack.SetActive(true);
                jump.SetActive(false);
                actionTime = 0;

            }

        }


        if (attack.activeSelf)
        {
            text.text = "Press 5 times";
            if (input.IsAttack && !down)
            {
                down = true;
                actionTime++;
            }
            if (!input.IsAttack)
            {
                down = false;
            }
            if (actionTime == 5)
            {
                attack.SetActive(false);
                useItem.SetActive(true);
                actionTime = 0;
                text.text = "Press S+G";
            }
        }
        if (useItem.activeSelf && input.ItemActive)
        {
            text.enabled = false;
            StartCoroutine("TutorialMove");

        }

    }
}

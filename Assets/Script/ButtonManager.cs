using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Events;

public class ButtonManager : MonoBehaviour
{
    //   private int levelAmount;
    public GameObject optionPanel;
    private PlayerInfo info;
    void Start()
    {

        if (GameObject.FindGameObjectWithTag("Player")) info = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerInfo>();
        //   levelAmount = GameObject.FindObjectsOfType<Button>().Length - 1;


        if (SceneManager.GetActiveScene().name.Equals("SelectLevel"))
        {
            GameObject listLevel = GameObject.Find("LevelBtn");
            for (int i = listLevel.transform.childCount - 1; i >= PlayerPrefs.GetInt("HighestLevel"); i--)
            {
                listLevel.transform.GetChild(i).gameObject.GetComponent<Button>().interactable = false;

            }

        }
    }
    public void DeleteData()
    {
        PlayerPrefs.DeleteAll();
    }
    public void GameMenuButtons()
    {
        string btnName;
        btnName = gameObject.name;

        if (btnName.Equals("PlayGameBtn"))
        {
            SceneManager.LoadScene("SelectMode");
        }
        if (btnName.Equals("ShopBtn"))
        {
            SceneManager.LoadScene("Shop");
        }
        if (btnName.Equals("SettingBtn"))
        {
            SceneManager.LoadScene("Setting");
        }

    }
    public void BackButton()
    {
        string btnName;
        btnName = gameObject.name;
        if (btnName.Equals("BackBtn"))
        {
            SceneManager.LoadScene("GameMenu");
        }
    }
    public void SelectModeButtons()
    {
        string btnName;
        btnName = gameObject.name;
        if (btnName.Equals("NewGameBtn"))
        {
            PlayerPrefs.SetInt("Stun", 1);
            PlayerPrefs.SetInt("Bomb", 1);
            PlayerPrefs.SetInt("CurrentLevel", 1);
            PlayerPrefs.SetInt("AmountOfGold", 500);
            PlayerPrefs.SetInt("AmountOfLife", 5);
            PlayerPrefs.Save();

            if (!PlayerPrefs.HasKey("AmountOfLife"))
            { //SceneManager.LoadScene("Tutorial"); 
                SceneManager.LoadScene("Level1");
            }
            else
            {
                SceneManager.LoadScene("Level1");
            }
        }

        if (btnName.Equals("ContinueBtn"))
        {
            SceneManager.LoadScene("Level" + PlayerPrefs.GetInt("CurrentLevel").ToString());
        }
        if (btnName.Equals("SelectLevelBtn"))
        {
            SceneManager.LoadScene("SelectLevel");
        }
    }
    public void SelectLevelButtons()
    {
        string btnName;
        btnName = gameObject.name;
        if (btnName.Equals("CancelBtn"))
        {
            SceneManager.LoadScene("SelectMode");
        }
        else
        {
            PlayerPrefs.DeleteAll();
            PlayerPrefs.SetInt("AmountOfLife", 1);
            PlayerPrefs.Save();
            if (btnName.Equals("Lv2Btn")) SceneManager.LoadScene("Level2");
            if (btnName.Equals("Lv1Btn")) SceneManager.LoadScene("Level1");
            if (btnName.Equals("Lv3Btn")) SceneManager.LoadScene("Level3");

        }

    }
    public void UIButtons()
    {
        if (gameObject.name.Equals("OptionBtn"))
        {
            optionPanel.transform.localPosition = new Vector3(optionPanel.transform.localPosition.x, 0, optionPanel.transform.localPosition.z);
            Time.timeScale = 0;
            gameObject.GetComponent<Button>().interactable = false;
            return;
        }
        if (gameObject.name.Equals("ResumeBtn"))
        {
            Time.timeScale = 1;
            optionPanel.transform.localPosition = new Vector3(optionPanel.transform.localPosition.x, 1000, optionPanel.transform.localPosition.z);
            GameObject.Find("OptionBtn").GetComponent<Button>().interactable = true;
            return;
        }
        if (gameObject.name.Equals("HomeBtn"))
        {
            Time.timeScale = 1;
            if (!SceneManager.GetActiveScene().name.Equals("LoseScene"))
            {
                info.AmountOfLife--;
                info.SaveGame();
            }
            else
            {
                PlayerPrefs.DeleteAll();
            }
            SceneManager.LoadScene("GameMenu");

            return;
        }
        if (gameObject.name.Equals("RestartBtn"))
        {
            info.AmountOfLife--;
            Time.timeScale = 1;
            if (info.AmountOfLife < 1)
            {
                SceneManager.LoadScene("LoseScene");
                return;
            }
            SceneManager.LoadScene("Level" + info.CurrentLevel.ToString());
            info.SaveGame();
            return;
        }
    }

}

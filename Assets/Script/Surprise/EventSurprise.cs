using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class EventSurprise : MonoBehaviour {
    public UnityEvent  boxAppear,boxDisappear;
	

     void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            boxAppear.Invoke();
        }
       
    }
    void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player")) {
            boxDisappear.Invoke();
        }
    }
}

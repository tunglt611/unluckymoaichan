using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class SupriseElement : MonoBehaviour
{
    private GameObject hiddenEnemy, hiddenBox, hiddenDead;
    private Animator hE_anim;
    public Transform playerTr;
    public Transform belowBox;
    public GameObject enemy;
    string nameOfScene;
    public Transform enemies;
    // Use this for initialization
    void Start()
    {
        nameOfScene = SceneManager.GetActiveScene().name;
        hiddenEnemy = transform.GetChild(0).gameObject;
        hiddenBox = transform.GetChild(1).gameObject;
        hiddenDead = transform.GetChild(2).gameObject;
        hiddenBox.SetActive(false);
        hiddenDead.SetActive(false);
        hE_anim = hiddenEnemy.GetComponent<Animator>();
        hE_anim.enabled = false;
    }
    void Update()
    {
        if (nameOfScene == "Level1" && (playerTr.position.x < 0 || playerTr.position.y < 0.1f))
        {
            BoxDisappear();
        }
        if (nameOfScene == "Level2" && (playerTr.position.x < -8.4f || playerTr.position.x > -6.7f || playerTr.position.y < 2.5f))
        {
            BoxDisappear();
        }


        DeadAppear();
    }

    public void BoxAppear()
    {
        if (belowBox != null && nameOfScene == "Level1") hiddenBox.transform.position = new Vector3(hiddenBox.transform.position.x, belowBox.position.y + 0.92f, hiddenBox.transform.position.z);

        hiddenBox.SetActive(true);
    }
    public void BoxDisappear()
    {
        hiddenBox.SetActive(false);
    }
    public void HeAtk()
    {
        hE_anim.enabled = true;
    }
    public void DeadAppear()
    {
        if (nameOfScene == "Level1")
        {
            if (playerTr.position.x > 5 && playerTr.position.y > 1.85f)
                hiddenDead.SetActive(true);
            else hiddenDead.SetActive(false);
        }
        if (nameOfScene == "Level2")
        {
            if (playerTr.position.x < 0.23f && playerTr.position.x > -1f && playerTr.position.y > -0.3f && playerTr.position.y < 0.9f)
                hiddenDead.SetActive(true);
            else hiddenDead.SetActive(false);
        }

    }
    public void EnemyBorn(Transform enemyTr) {
        StartCoroutine("EnemyBornIE",  enemyTr);
       
    }

    IEnumerator EnemyBornIE(Transform enemyTr) {
        yield return new WaitForSeconds(0.2f);
        for (int i = 0; i < 5; i++)
        {
            GameObject enemyClone = Instantiate(enemy, enemyTr.position, Quaternion.identity, enemies);
            enemyClone.GetComponent<MonsterMovement>().Speed = Random.Range(0.02f,0.06f);
        }
    }

}

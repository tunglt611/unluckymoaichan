using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
public class DoorOpen : MonoBehaviour
{
    private Animator doorAnim;
    private bool done;
    public GameObject winPanel;
    public UnityEvent doorOpen, nextLevel;
    public GameObject objects;
    private int numOfObj;
    // Use this for initialization
    void Start()
    {
        if (objects != null)
        {
            numOfObj = objects.transform.childCount;
        }
        else
        {
            numOfObj = 0;
        }
        doorAnim = GetComponent<Animator>();
        doorAnim.enabled = false;
        done = false;
    }

    // Update is called once per frame

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (numOfObj < 1 && done == false)
        {
            doorAnim.enabled = true;
            done = true;
            doorOpen.Invoke();
            StartCoroutine("NextLevel");
        }
    }
    IEnumerator NextLevel()
    {
        int currentLevel = PlayerPrefs.GetInt("CurrentLevel") + 1;
        PlayerPrefs.SetInt("CurrentLevel", currentLevel);
        if (currentLevel > PlayerPrefs.GetInt("HighestLevel")) PlayerPrefs.SetInt("HighestLevel", currentLevel);
        PlayerPrefs.Save();
        yield return new WaitForSeconds(2.0f);
        nextLevel.Invoke();
        done = false;
        winPanel.SetActive(true);
        yield return new WaitForSeconds(3.0f);
        if (currentLevel < 4)
            SceneManager.LoadScene("Level" + currentLevel.ToString());
        else
        {
            SceneManager.LoadScene("GameMenu");
        }
    }
    public void CountObj()
    {
        numOfObj--;
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class HiddenEnemyEvent : MonoBehaviour {
    public UnityEvent playerCome;


     void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player")) {
            float x = gameObject.transform.position.x - collision.gameObject.transform.position.x;
            if (x > 0) gameObject.transform.eulerAngles = new Vector3(0,180,0); 
            playerCome.Invoke();
        }
        
    }
     void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("AttackPath"))
        {
            gameObject.SetActive(false);
        }
    }
}

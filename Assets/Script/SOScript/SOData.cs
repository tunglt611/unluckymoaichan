using System.Collections.Generic;
using UnityEngine;
using System;
[CreateAssetMenu(fileName ="PlayerData",menuName ="Data",order = 0)]
[Serializable]
public class SOData : ScriptableObject {
    public int level;
    public bool playedTutorial;
    List<ScriptableObject> listSO;


}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputMobile : MonoBehaviour {
    PlayerInput input;

    // Use this for initialization
    void Start () {
   input = GameObject.FindWithTag("Player").GetComponent<PlayerInput>();
    }

    // Update is called once per frame
    public bool IsForward
    {
        get
        {
            //Some other code
            return input.IsForward;
        }
        set
        {
            //Some other code
            input.IsForward = value;
        }
    }
    public bool IsJump
    {
        get
        {
            //Some other code
            return input.IsJump;
        }
        set
        {
            //Some other code
            input.IsJump = value;
        }
    }
    public bool IsBack
    {

        get
        {
            //Some other code
            return input.IsBack;
        }
        set
        {
            //Some other code
            input.IsBack = value;
        }
    }
    public bool IsAttack
    {
        get
        {
            //Some other code
            return input.IsAttack;
        }
        set
        {
            //Some other code
            input.IsAttack = value;
        }
    }
    public bool ItemActive
    {
        get
        {
            //Some other code
            return input.ItemActive;
        }
        set
        {
            //Some other code
            input.ItemActive = value;
        }
    }
}
